'use strict';
module.exports = (sequelize, DataTypes) => {
  const inventory = sequelize.define('inventory', {
    name: DataTypes.STRING,
    qty: DataTypes.INTEGER
  }, {});
  inventory.associate = function(models) {
    // associations can be defined here
  };
  return inventory;
};